﻿using MailKit.Security;
using MimeKit;
using MailKit.Net.Smtp;

namespace BE.Helpers
{
    public class MailHelpers
    {
        public static async Task Send(string toName, string toEmail, string subject, string messageText)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("No Reply", "info.dlanguage@gmail.com"));
            message.To.Add(new MailboxAddress(toName, toEmail));
            message.Subject = subject;
            message.Importance = MessageImportance.High;
            message.Priority = MessagePriority.Urgent;
            message.XPriority = XMessagePriority.Highest;
            message.Body = new TextPart("html")
            {
                Text = messageText,
            };

            using (var smtp = new SmtpClient())
            {
                await smtp.ConnectAsync("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
                await smtp.AuthenticateAsync("info.dlanguage@gmail.com", "wwjdkttswydqubig");
                await smtp.SendAsync(message);
                await smtp.DisconnectAsync(true);
            }
        }
    }
}
