﻿namespace BE.Dtos.Payment
{
    public class PaymentDto
    {
        public string PaymentMethod { get; set; } = string.Empty;
        public IFormFile? Logo { get; set; }
        public bool IsActive { get; set; }
    }
}
