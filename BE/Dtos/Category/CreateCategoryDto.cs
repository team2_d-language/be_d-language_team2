﻿namespace BE.Dtos.Course
{
    public class CreateCategoryDto
    {
        public string CategoryName { get; set; } = string.Empty;
        public IFormFile Banner { get; set; }
        public IFormFile Flag { get; set; }
        public string Description { get; set; } = string.Empty;
    }
}
