﻿namespace BE.Dtos.Course
{
    public class UpdateCategoryDto
    {
        public string CategoryName { get; set; } = string.Empty;
        public IFormFile? Banner { get; set; }
        public IFormFile? Flag { get; set; }
        public string Description { get; set; } = string.Empty;
        public bool IsActive { get; set; } = true;
    }
}
