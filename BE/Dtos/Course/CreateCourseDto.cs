﻿namespace BE.Dtos.Course
{
    public class CreateCourseDto
    {
        public string Name { get; set; } = string.Empty;
        public string Category_id { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public int Price { get; set; }
        public IFormFile? Image { get; set; }
    }
}
