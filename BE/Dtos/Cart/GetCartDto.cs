﻿namespace BE.Dtos.Cart
{
    public class GetCartDto
    {
        public string Id { get; set; } = string.Empty;
        public string UserId { get; set; } = string.Empty;
        public string CourseId { get; set; } = string.Empty;
        public string CourseName {  get; set; } = string.Empty;
        public string CourseImage { get; set; } = string.Empty;
        public int CoursePrice { get; set; }
        public DateTime Schedule { get; set; }
        public string CategoryName { get; set; } = string.Empty;
    }
}
