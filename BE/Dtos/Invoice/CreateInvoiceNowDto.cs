﻿namespace BE.Dtos.Invoice
{
    public class CreateInvoiceNowDto
    {
        public string PaymentId { get; set; } = string.Empty;
        public string CourseId { get; set; } = string.Empty;
        public DateTime Schedule { get; set; }
    }
}
