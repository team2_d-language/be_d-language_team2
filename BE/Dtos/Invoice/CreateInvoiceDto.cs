﻿namespace BE.Dtos.Invoice
{
    public class CreateInvoiceDto
    {
        public string PaymentId { get; set; } = string.Empty;
        public List<string> CartIds { get; set; } = new List<string>();
    }
}
