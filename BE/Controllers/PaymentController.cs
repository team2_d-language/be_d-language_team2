﻿using BE.Dtos.Payment;
using BE.Models;
using BE.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace BE.Controllers
{
    [Route("/payments")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly PaymentRepository _paymentRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public PaymentController(PaymentRepository PaymentRepository, IWebHostEnvironment webHostEnvironment)
        {
            _paymentRepository = PaymentRepository;
            _webHostEnvironment = webHostEnvironment;
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public ActionResult GetAll()
        {
            return Ok(_paymentRepository.GetAll());
        }

        [Authorize]
        [HttpGet("active")]
        public ActionResult GetAllActive()
        {
            return Ok(_paymentRepository.GetAllActive());
        }

        [Authorize(Roles = "admin")]
        [HttpGet("{id}")]
        public ActionResult GetById(string id)
        {
            Payment payment = _paymentRepository.GetById(id);
            if (payment.Id == "")
            {
                return NotFound();
            }
            return Ok(payment);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<ActionResult> Create([FromForm] PaymentDto paymentBody)
        {
            try
            {
                IFormFile image = paymentBody.Logo!;

                // TODO: Save image to the server
                var extImage = Path.GetExtension(image.FileName).ToLowerInvariant();

                // Get filename
                string imageFileName = Guid.NewGuid().ToString() + extImage;

                string uploadDir = "uploads";
                string physicalPath = $"wwwroot/{uploadDir}";

                // Saving image
                var imageFilePath = Path.Combine(_webHostEnvironment.ContentRootPath, physicalPath, imageFileName);

                using var streamImage = System.IO.File.Create(imageFilePath);
                await image.CopyToAsync(streamImage);

                // Create URL path
                string imageUrlPath = $"{uploadDir}/{imageFileName}";
                string errorMessage = _paymentRepository.Create(new Payment
                {
                    PaymentMethod = paymentBody.PaymentMethod,
                    Logo = imageUrlPath,
                    IsActive = paymentBody.IsActive,
                });

                if (string.IsNullOrEmpty(errorMessage))
                {
                    return StatusCode(201, new { code = HttpStatusCode.Created, message = "Successfully create new payment" });
                }
                else
                {
                    return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = errorMessage });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = $"An error occurred: {ex.Message}" });
            }
        }

        [Authorize(Roles = "admin")]
        [HttpPut("{id}")]
        public async Task<ActionResult> Update(string id, [FromForm] PaymentDto paymentBody)
        {
            try
            {
                Payment payment = _paymentRepository.GetById(id);
                if (payment.Id == "") return NotFound(new { message = "Payment Not Found" });

                string errorMessage;
                if (paymentBody.Logo != null)
                {
                    IFormFile image = paymentBody.Logo!;

                    // TODO: Save image to the server
                    var extImage = Path.GetExtension(image.FileName).ToLowerInvariant();

                    // Get filename
                    string imageFileName = Guid.NewGuid().ToString() + extImage;

                    string uploadDir = "uploads";
                    string physicalPath = $"wwwroot/{uploadDir}";

                    // Saving image
                    var imageFilePath = Path.Combine(_webHostEnvironment.ContentRootPath, physicalPath, imageFileName);

                    using var streamImage = System.IO.File.Create(imageFilePath);
                    await image.CopyToAsync(streamImage);

                    // Create URL path
                    string imageUrlPath = $"{uploadDir}/{imageFileName}";

                    errorMessage = _paymentRepository.Update(id, new Payment
                    {
                        PaymentMethod = paymentBody.PaymentMethod,
                        Logo = imageUrlPath,
                        IsActive = paymentBody.IsActive,
                    });
                }
                else
                {
                    errorMessage = _paymentRepository.Update(id, new Payment
                    {
                        PaymentMethod = paymentBody.PaymentMethod,
                        Logo = payment.Logo,
                        IsActive = paymentBody.IsActive,
                    });
                }

                if (string.IsNullOrEmpty(errorMessage))
                {
                    Payment updatedPayment = _paymentRepository.GetById(id);
                    return StatusCode(200, new { code = HttpStatusCode.OK, message = "Successfully update Payment", updatedPayment });
                }
                else
                {
                    return StatusCode(400, new { code = HttpStatusCode.BadRequest, message = errorMessage });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = $"An error occurred: {ex.Message}" });
            }
        }

        [Authorize(Roles = "admin")]
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            Payment payment = _paymentRepository.GetById(id);
            if (payment.Id == "") return NotFound(new { message = "Payment Not Found" });
            _paymentRepository.Delete(id);
            return Ok(new { message = "Successfully Delete Payment" });
        }
    }
}
