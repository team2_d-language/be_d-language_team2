﻿using BE.Dtos.Course;
using BE.Dtos.Invoice;
using BE.Models;
using BE.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Security.Claims;

namespace BE.Controllers
{
    [ApiController]
    [Route("/invoice")]
    public class InvoiceController : ControllerBase
    {
        private readonly InvoiceRepository _invoiceRepository;
        private readonly PaymentRepository _paymentRepository;
        private readonly CourseRepository _courseRepository;

        public InvoiceController(InvoiceRepository invoiceRepository, PaymentRepository paymentRepository, CourseRepository courseRepository)
        {
            _invoiceRepository = invoiceRepository;
            _paymentRepository = paymentRepository;
            _courseRepository = courseRepository;
        }

        [Authorize]
        [HttpPost]
        public ActionResult CreateInvoice([FromBody] CreateInvoiceDto invoiceDto)
        {
            try
            {
                var userIdClaim = User.FindFirst(ClaimTypes.Sid);

                if (userIdClaim == null || string.IsNullOrEmpty(userIdClaim.Value))
                {
                    return StatusCode(404, "User ID not found.");
                }

                Payment payment = _paymentRepository.GetById(invoiceDto.PaymentId);
                if (payment.Id == "")
                {
                    return StatusCode(404, "Payment not found.");
                }

                string errorMessage = _invoiceRepository.CreateInvoice(invoiceDto.PaymentId, userIdClaim.Value, invoiceDto.CartIds);

                if (string.IsNullOrEmpty(errorMessage))
                {
                    return StatusCode(201, new { code = HttpStatusCode.Created, message = "Payment success." });
                }
                else
                {
                    return StatusCode(400, new { code = HttpStatusCode.BadRequest, message = errorMessage });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }

        [Authorize]
        [HttpPost("/invoice/now")]
        public ActionResult CreateInvoiceNow([FromBody] CreateInvoiceNowDto invoiceNowDto)
        {
            try
            {
                var userIdClaim = User.FindFirst(ClaimTypes.Sid);

                if (userIdClaim == null || string.IsNullOrEmpty(userIdClaim.Value))
                {
                    return StatusCode(404, "User ID not found.");
                }

                Payment payment = _paymentRepository.GetById(invoiceNowDto.PaymentId);
                if (payment.Id == "")
                {
                    return StatusCode(404, "Payment not found.");
                }

                GetAllCourseDto course = _courseRepository.GetByIdActive(invoiceNowDto.CourseId);
                if (course.Id == "")
                {
                    return StatusCode(404, "Course not found.");
                }


                string errorMessage = _invoiceRepository.CreateInvoiceNow(invoiceNowDto.PaymentId, userIdClaim.Value, course, invoiceNowDto.Schedule);

                if (string.IsNullOrEmpty(errorMessage))
                {
                    return StatusCode(201, new { code = HttpStatusCode.Created, message = "Payment success." });
                }
                else
                {
                    return StatusCode(400, new { code = HttpStatusCode.BadRequest, message = errorMessage });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }

        [Authorize(Roles = "admin")]
        [HttpGet("/invoice-all")]
        public ActionResult getInvoices()
        {
            return Ok(_invoiceRepository.GetAll());
        }

        [Authorize]
        [HttpGet]
        public ActionResult getInvoiceByUserId()
        {
            var userIdClaim = User.FindFirst(ClaimTypes.Sid);

            if (userIdClaim == null || string.IsNullOrEmpty(userIdClaim.Value))
            {
                return StatusCode(404, "User ID not found.");
            }
            return Ok(_invoiceRepository.GetAllByUserId(userIdClaim.Value));
        }

        [Authorize]
        [HttpGet("{id}")]
        public ActionResult getInvoiceDetails(string id)
        {
            Invoice invoiceDetails = _invoiceRepository.GetInvoiceById(id);
            if (invoiceDetails == null || invoiceDetails.Id == "")
            {
                return BadRequest("Invalid id");
            }
            return Ok(invoiceDetails);
        }
    }
}
