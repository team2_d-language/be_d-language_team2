﻿using BE.Dtos.User;
using BE.Helpers;
using BE.Models;
using BE.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace BE.Controllers
{
    [Route("/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserRepository _userRepository;

        public UserController(UserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public ActionResult GetAll()
        {
            List<User> users = _userRepository.GetAll();

            return Ok(new
            {
                code = HttpStatusCode.OK,
                message = "Successfully Fetch User",
                users
            });
        }

        [Authorize(Roles = "admin")]
        [HttpGet("{id}")]
        public ActionResult GetById(string id)
        {
            User user = _userRepository.GetById(id);
            if (user.Id == "")
            {
                return NotFound();
            }
            return Ok(new
            {
                code = HttpStatusCode.OK,
                message = "Successfully Fetch User",
                user
            });
        }

        [HttpPost("/create-admin")]
        public ActionResult CreateAdmin([FromBody] RegisterUserDto userBody)
        {
            string verificationToken = Guid.NewGuid().ToString();
            try
            {
                string errorMessage = _userRepository.CreateAdmin(new User
                {
                    FullName = userBody.FullName,
                    Email = userBody.Email.ToLower(),
                    Password = userBody.Password,
                    ConfirmPassword = userBody.ConfirmPassword
                });
                if (string.IsNullOrEmpty(errorMessage))
                {
                    return StatusCode(201, new { code = HttpStatusCode.Created, message = "Successfully Create Admin" });
                }
                else
                {
                    return StatusCode(400, new { code = HttpStatusCode.BadRequest, message = errorMessage });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = $"An error occurred: {ex.Message}" });
            }
        }

        [HttpPost("/register")]
        public async Task<ActionResult> Register([FromBody] RegisterUserDto userBody)
        {
            string verificationToken = Guid.NewGuid().ToString();
            try
            {
                string errorMessage = _userRepository.Register(new User
                {
                    FullName = userBody.FullName,
                    Email = userBody.Email.ToLower(),
                    Password = userBody.Password,
                    ConfirmPassword = userBody.ConfirmPassword
                }, verificationToken);
                if (string.IsNullOrEmpty(errorMessage))
                {
                    // string htmlEmail = $@"
                    // Hello <b>{userBody.Email.ToLower()}</b>, please click link below to verify your email<br/>
                    // <a href='http://52.237.194.35:2027/confirm/{verificationToken}'>Verify My Account</a>
                    // ";
                    string htmlEmail = $@"
                    Hello <b>{userBody.Email.ToLower()}</b>, please click link below to verify your email<br/>
                    <a href='http://localhost:5173/confirm/{verificationToken}'>Verify My Account</a>
                    ";
                    await MailHelpers.Send("Dear User", userBody.Email.ToLower(), "Email Verification", htmlEmail);
                    return StatusCode(201, new { code = HttpStatusCode.Created, message = "Congratulations, your account has been successfull created. Please check your email for verification." });
                }
                else
                {
                    return StatusCode(400, new { code = HttpStatusCode.InternalServerError, message = errorMessage });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = $"An error occurred: {ex.Message}" });
            }
        }

        [HttpPost("/verify")]
        public ActionResult Verify([FromBody] VerifyDto data)
        {
            User? user = _userRepository.GetByToken(data.Token);
            if (user == null)
            {
                return NotFound();
            }

            string errorMessage = _userRepository.Activate(user.Id);
            if (!string.IsNullOrEmpty(errorMessage))
            {
                return Problem(errorMessage);
            }

            return Ok();
        }

        [Authorize(Roles = "admin")]
        [HttpPut("{id}")]
        public ActionResult Update(string id, [FromBody] UpdateUserDto userBody)
        {
            try
            {
                User user = _userRepository.GetById(id);
                if (user.Id == "") return NotFound(new { code = HttpStatusCode.NotFound, message = "User Not Found" });

                string errorMessage = _userRepository.Update(id, new User
                {
                    FullName = userBody.FullName,
                    Email = userBody.Email.ToLower(),
                    Role = userBody.Role,
                    IsActive = userBody.IsActive,
                });
                if (string.IsNullOrEmpty(errorMessage))
                {
                    User updatedUser = _userRepository.GetById(id);
                    return StatusCode(200, new { code = HttpStatusCode.OK, message = "Successfully update new user", updatedUser });
                }
                else
                {
                    return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = errorMessage });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = $"An error occurred: {ex.Message}" });
            }
        }

        [Authorize(Roles = "admin")]
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            User user = _userRepository.GetById(id);
            if (user.Id == "") return NotFound(new { code = HttpStatusCode.NotFound, message = "User Not Found" });
            _userRepository.Delete(id);
            return Ok(new { code = HttpStatusCode.OK, message = "Successfully Delete User" });
        }

        [HttpPost("/reset-password")]
        public async Task<ActionResult> ResetPassword([FromBody] ResetPasswordDto data)
        {
            string verificationToken = Guid.NewGuid().ToString();

            if (_userRepository.IsEmailExist(data.Email))
            {
                if (_userRepository.ResetPassword(data.Email, verificationToken))
                {
                    // string htmlEmail = $@"
                    //     Hello <b>{data.Email}</b>, please click link below to reset your password<br/>
                    //     <a href='http://52.237.194.35:2027/new-password/{verificationToken}'>Reset Password</a>
                    //     ";
                    string htmlEmail = $@"
                       Hello <b>{data.Email}</b>, please click link below to reset your password<br/>
                       <a href='http://localhost:5173/new-password/{verificationToken}'>Reset Password</a>
                       ";
                    await MailHelpers.Send("Dear User", data.Email, "Forget Password", htmlEmail);
                    return Ok(new { code = HttpStatusCode.OK, message = "Please check your email" });
                }
                return BadRequest(new { code = HttpStatusCode.BadRequest, message = "Something went wrong" });
            }
            return NotFound(new { code = HttpStatusCode.NotFound, message = "Email not registered" });
        }

        [HttpPut("/create-password")]
        public ActionResult CreatePassword([FromBody] CreatePasswordDto createPasswordBody)
        {
            if (!_userRepository.FindTokenResetPassword(createPasswordBody.Token))
            {
                return BadRequest(new { code = 400, message = "Invalid token" });
            }
            string message = _userRepository.CreatePassword(createPasswordBody);
            if (message == "Successfully reset password")
            {
                return Ok(new { code = 200, message });
            }
            return BadRequest(new { code = HttpStatusCode.BadRequest, message });
        }
    }
}
