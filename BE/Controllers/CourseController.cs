﻿using BE.Dtos.Course;
using BE.Models;
using BE.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Security.Claims;

namespace BE.Controllers
{
    [Route("/courses")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly CourseRepository _courseRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public CourseController(CourseRepository courseRepository, IWebHostEnvironment webHostEnvironment)
        {
            _courseRepository = courseRepository;
            _webHostEnvironment = webHostEnvironment;
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public ActionResult GetAll()
        {
            List<Course> courses = _courseRepository.GetAll();

            return Ok(new
            {
                code = HttpStatusCode.OK,
                message = "Successfully Fetch Course",
                courses
            });
        }

        [HttpGet("active")]
        public ActionResult GetAllActive()
        {
            List<GetAllCourseDto> courses = _courseRepository.GetAllActive();

            return Ok(new
            {
                code = HttpStatusCode.OK,
                message = "Successfully Fetch Course",
                courses
            });
        }

        [HttpGet("category/{category}")]
        public ActionResult GetAllByType(string category)
        {
            List<GetAllCourseDto> courses = _courseRepository.GetAllByCategoryActive(category);

            return Ok(new
            {
                code = HttpStatusCode.OK,
                message = "Successfully Fetch Course",
                courses
            });
        }

        [Authorize(Roles = "admin")]
        [HttpGet("category-all/{category}")]
        public ActionResult GetAllCourseByType(string category)
        {
            List<GetAllCourseDto> courses = _courseRepository.GetAllByCategory(category);

            return Ok(new
            {
                code = HttpStatusCode.OK,
                message = "Successfully Fetch Course",
                courses
            });
        }

        [Authorize]
        [HttpGet("/course-active/{id}")]
        public ActionResult GetByIdActive(string id)
        {
            GetAllCourseDto course = _courseRepository.GetByIdActive(id);
            if (course.Id == "")
            {
                return NotFound();
            }
            return Ok(new
            {
                code = HttpStatusCode.OK,
                message = "Successfully Fetch Course",
                course
            });
        }

        [Authorize(Roles = "admin")]
        [HttpGet("{id}")]
        public ActionResult GetById(string id)
        {
            GetAllCourseDto course = _courseRepository.GetById(id);
            if (course.Id == "")
            {
                return NotFound();
            }
            return Ok(new
            {
                code = HttpStatusCode.OK,
                message = "Successfully Fetch Course",
                course
            });
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<ActionResult> Create([FromForm] CreateCourseDto courseBody)
        {
            try
            {
                IFormFile image = courseBody.Image!;

                // TODO: Save image to the server
                var extImage = Path.GetExtension(image.FileName).ToLowerInvariant();

                // Get filename
                string imageFileName = Guid.NewGuid().ToString() + extImage;

                string uploadDir = "uploads";
                string physicalPath = $"wwwroot/{uploadDir}";

                // Saving image
                var imageFilePath = Path.Combine(_webHostEnvironment.ContentRootPath, physicalPath, imageFileName);

                using var streamImage = System.IO.File.Create(imageFilePath);
                await image.CopyToAsync(streamImage);

                // Create URL path
                string imageUrlPath = $"{uploadDir}/{imageFileName}";
                string errorMessage = _courseRepository.Create(new Course
                {
                    Name = courseBody.Name,
                    Category_id = courseBody.Category_id,
                    Description = courseBody.Description,
                    Price = courseBody.Price,
                    Image = imageUrlPath,
                });
                if (string.IsNullOrEmpty(errorMessage))
                {
                    return StatusCode(201, new { code = HttpStatusCode.Created, message = "Successfully create new course" });
                }
                else
                {
                    return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = errorMessage });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = $"An error occurred: {ex.Message}" });
            }
        }

        [Authorize(Roles = "admin")]
        [HttpPut("{id}")]
        public async Task<ActionResult> Update(string id, [FromForm] UpdateCourseDto courseBody)
        {
            try
            {
                GetAllCourseDto course = _courseRepository.GetById(id);
                if (course.Id == "") return NotFound(new { code = HttpStatusCode.NotFound, message = "Course Not Found" });

                string errorMessage;

                if (courseBody.Image != null)
                {
                    IFormFile image = courseBody.Image!;

                    // TODO: Save image to the server
                    var extImage = Path.GetExtension(image.FileName).ToLowerInvariant();

                    // Get filename
                    string imageFileName = Guid.NewGuid().ToString() + extImage;

                    string uploadDir = "uploads";
                    string physicalPath = $"wwwroot/{uploadDir}";

                    // Saving image
                    var imageFilePath = Path.Combine(_webHostEnvironment.ContentRootPath, physicalPath, imageFileName);

                    using var streamImage = System.IO.File.Create(imageFilePath);
                    await image.CopyToAsync(streamImage);

                    // Create URL path
                    string imageUrlPath = $"{uploadDir}/{imageFileName}";

                    errorMessage = _courseRepository.Update(id, new Course
                    {
                        Name = courseBody.Name,
                        Category_id = courseBody.Category_id,
                        Description = courseBody.Description,
                        Price = courseBody.Price,
                        Image = imageUrlPath,
                        IsActive = courseBody.IsActive,
                    });
                }
                else
                {
                    errorMessage = _courseRepository.Update(id, new Course
                    {
                        Name = courseBody.Name,
                        Category_id = courseBody.Category_id,
                        Description = courseBody.Description,
                        Price = courseBody.Price,
                        Image = course.Image,
                        IsActive = courseBody.IsActive,
                    });
                }

                if (string.IsNullOrEmpty(errorMessage))
                {
                    GetAllCourseDto updatedCourse = _courseRepository.GetById(id);
                    return StatusCode(200, new { code = HttpStatusCode.OK, message = "Successfully update course", updatedCourse });
                }
                else
                {
                    return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = errorMessage });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = $"An error occurred: {ex.Message}" });
            }
        }

        [Authorize(Roles = "admin")]
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            GetAllCourseDto course = _courseRepository.GetById(id);
            if (course.Id == "") return NotFound(new { code = HttpStatusCode.NotFound, message = "Course Not Found" });
            _courseRepository.Delete(id);
            return Ok(new { code = HttpStatusCode.OK, message = "Successfully Delete Course" });
        }
    }
}
