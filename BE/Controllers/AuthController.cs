﻿using BE.Dtos.Auth;
using BE.Helpers;
using BE.Models;
using BE.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace BE.Controllers
{
    [Route("/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserRepository _userRepository;

        public AuthController(UserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpPost("/login")]
        public ActionResult Login([FromBody] LoginDto data)
        {
            string hashedPassword = PasswordHelper.HashPassword(data.Password);
            if(!_userRepository.IsEmailExist(data.Email.ToLower()))
            {
                return BadRequest(new { code = HttpStatusCode.BadRequest, message = "Invalid email or password" });
            }
            if (!_userRepository.IsEmailActive(data.Email.ToLower()))
            {
                return BadRequest(new { code = HttpStatusCode.BadRequest, message = "Your email is inactive, please verify your email!" });
            }

            User? user = _userRepository.GetByEmailAndPassword(data.Email.ToLower(), hashedPassword);

            if (user == null)
            {
                return BadRequest(new {code = HttpStatusCode.BadRequest, message = "Invalid email or password"});
            }

            string token = JWTHelper.Generate(user.Id, user.Role);

            return Ok(new{ token, role = user.Role});
        }
    }
}
