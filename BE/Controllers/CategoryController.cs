﻿using BE.Dtos.Course;
using BE.Models;
using BE.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace BE.Controllers
{
    [Route("/categories")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly CategoryRepository _categoryRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public CategoryController(CategoryRepository categoryRepository, IWebHostEnvironment webHostEnvironment)
        {
            _categoryRepository = categoryRepository;
            _webHostEnvironment = webHostEnvironment;
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public ActionResult GetAll()
        {
            return Ok(_categoryRepository.GetAll());
        }

        [HttpGet("active")]
        public ActionResult GetAllActive()
        {
            List<Category> categories = _categoryRepository.GetAllActive();

            return Ok(new
            {
                code = HttpStatusCode.OK,
                message = "Successfully Fetch Categories",
                categories
            });
        }

        [Authorize]
        [HttpGet("/category-active/{id}")]
        public ActionResult GetByIdActive(string id)
        {
            Category category = _categoryRepository.GetByIdActive(id);
            if (category.Id == "")
            {
                return NotFound();
            }
            return Ok(category);
        }

        [Authorize(Roles = "admin")]
        [HttpGet("{id}")]
        public ActionResult GetById(string id)
        {
            Category category = _categoryRepository.GetById(id);
            if (category.Id == "")
            {
                return NotFound();
            }
            return Ok(category);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<ActionResult> Create([FromForm] CreateCategoryDto categoryBody)
        {
            try
            {
                IFormFile banner = categoryBody.Banner!;
                IFormFile flag = categoryBody.Flag!;

                // TODO: Save image to the server
                var extBanner = Path.GetExtension(banner.FileName).ToLowerInvariant();
                var extFlag = Path.GetExtension(flag.FileName).ToLowerInvariant();

                // Get filename
                string bannerFileName = Guid.NewGuid().ToString() + extBanner;
                string flagFileName = Guid.NewGuid().ToString() + extFlag;

                string uploadDir = "uploads";
                string physicalPath = $"wwwroot/{uploadDir}";

                // Saving image
                var bannerFilePath = Path.Combine(_webHostEnvironment.ContentRootPath, physicalPath, bannerFileName);
                var flagFilePath = Path.Combine(_webHostEnvironment.ContentRootPath, physicalPath, flagFileName);

                using var streamBanner = System.IO.File.Create(bannerFilePath);
                using var streamFlag = System.IO.File.Create(flagFilePath);
                await banner.CopyToAsync(streamBanner);
                await flag.CopyToAsync(streamFlag);

                // Create URL path
                string bannerUrlPath = $"{uploadDir}/{bannerFileName}";
                string flagUrlPath = $"{uploadDir}/{flagFileName}";

                string errorMessage = _categoryRepository.Create(new Category
                {
                    CategoryName = categoryBody.CategoryName,
                    Banner = bannerUrlPath,
                    Flag = flagUrlPath,
                    Description = categoryBody.Description,
                });

                if (string.IsNullOrEmpty(errorMessage))
                {
                    return StatusCode(201, new { code = HttpStatusCode.Created, message = "Successfully create new category" });
                }
                else
                {
                    return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = errorMessage });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = $"An error occurred: {ex.Message}" });
            }
        }

        [Authorize(Roles = "admin")]
        [HttpPut("{id}")]
        public async Task<ActionResult> Update(string id, [FromForm] UpdateCategoryDto categoryBody)
        {
            try
            {
                Category category = _categoryRepository.GetById(id);
                if (category.Id == "") return NotFound(new { message = "Category Not Found" });

                string errorMessage;
                if (categoryBody.Banner != null)
                {
                    IFormFile banner = categoryBody.Banner!;
                    IFormFile flag = categoryBody.Flag!;

                    // TODO: save image to server
                    var extBanner = Path.GetExtension(banner.FileName).ToLowerInvariant();
                    var extFlag = Path.GetExtension(flag.FileName).ToLowerInvariant();

                    //get filename
                    string bannerFileName = Guid.NewGuid().ToString() + extBanner;
                    string flagFileName = Guid.NewGuid().ToString() + extFlag;

                    string uploadDir = "uploads";
                    string physicalPath = $"wwwroot/{uploadDir}";
                    //saving image
                    var bannerFilePath = Path.Combine(_webHostEnvironment.ContentRootPath, physicalPath, bannerFileName);
                    var flagFilePath = Path.Combine(_webHostEnvironment.ContentRootPath, physicalPath, flagFileName);

                    using var streamBanner = System.IO.File.Create(bannerFilePath);
                    using var streamFlag = System.IO.File.Create(flagFilePath);
                    await banner.CopyToAsync(streamBanner);
                    await flag.CopyToAsync(streamFlag);

                    //create url path
                    string bannerUrlPath = $"{uploadDir}/{bannerFileName}";
                    string flagUrlPath = $"{uploadDir}/{flagFileName}";

                    errorMessage = _categoryRepository.Update(id, new Category
                    {
                        CategoryName = categoryBody.CategoryName,
                        Banner = bannerUrlPath,
                        Flag = flagUrlPath,
                        Description = categoryBody.Description,
                        IsActive = categoryBody.IsActive,
                    });
                } 
                else
                {
                    errorMessage = _categoryRepository.Update(id, new Category
                    {
                        CategoryName = categoryBody.CategoryName,
                        Banner = category.Banner,
                        Flag = category.Flag,
                        Description = categoryBody.Description,
                        IsActive = categoryBody.IsActive,
                    });
                }

                if (string.IsNullOrEmpty(errorMessage))
                {
                    Category updatedCategory = _categoryRepository.GetById(id);
                    return StatusCode(200, new { code = HttpStatusCode.OK, message = "Successfully update category", updatedCategory });
                }
                else
                {
                    return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = errorMessage });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = $"An error occurred: {ex.Message}" });
            }
        }

        [Authorize(Roles = "admin")]
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            Category category = _categoryRepository.GetById(id);
            if (category.Id == "") return NotFound(new { message = "Category Not Found" });
            _categoryRepository.Delete(id);
            return Ok(new { message = "Successfully Delete Category" });
        }
    }
}
