﻿using BE.Dtos;
using BE.Models;
using BE.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Security.Claims;

namespace BE.Controllers
{
    [ApiController]
    [Route("/carts")]
    public class CartController : ControllerBase
    {
        private readonly CartRepository _cartRepository;

        public CartController(CartRepository cartRepository)
        {
            _cartRepository = cartRepository;
        }

        [Authorize]
        [HttpPost]
        public ActionResult CreateCart([FromBody] CartDto cartDto)
        {
            try
            {
                var userIdClaim = User.FindFirst(ClaimTypes.Sid);

                if (userIdClaim == null || string.IsNullOrEmpty(userIdClaim.Value))
                {
                    return StatusCode(404, "User ID not found.");
                }

                var newCart = new Cart
                {
                    UserId = userIdClaim.Value,
                    CourseId = cartDto.CourseId,
                    Schedule = cartDto.Schedule,
                };

                string errorMessage = _cartRepository.CreateCart(newCart);

                if (string.IsNullOrEmpty(errorMessage))
                {
                    return StatusCode(201, new { code = HttpStatusCode.Created, message = "Successfully added to cart" });
                }
                else
                {
                    return StatusCode(400, new { code = HttpStatusCode.BadRequest, message = errorMessage });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { code = HttpStatusCode.InternalServerError, message = $"An error occurred: {ex.Message}" });
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult GetCartsByUserId()
        {
            try
            {
                var userIdClaim = User.FindFirst(ClaimTypes.Sid);

                if (userIdClaim != null)
                {
                    var user_id = userIdClaim.Value;
                    var carts = _cartRepository.GetCartsByUserId(user_id);

                    if (carts != null && carts.Any())
                    {
                        return Ok(carts);
                    }
                    else
                    {
                        return NotFound($"No carts found for user with ID {user_id}");
                    }
                }
                else
                {
                    // Handle the case where the user ID claim is not found in the token
                    return BadRequest("User ID claim not found in the token.");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }

        [Authorize]
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            _cartRepository.Delete(id);
            return Ok(new { message = "Successfully Delete Cart" });
        }
    }
}
