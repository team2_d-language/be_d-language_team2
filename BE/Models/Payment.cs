﻿namespace BE.Models
{
    public class Payment
    {
        public string Id { get; set; } = string.Empty;
        public string PaymentMethod { get; set; } = string.Empty;
        public string Logo { get; set; } = string.Empty;
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set;}
    }
}
