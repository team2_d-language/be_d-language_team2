﻿namespace BE.Models
{
    public class User
    {
        public string Id { get; set; } = string.Empty;
        public string FullName { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public string ConfirmPassword { get; set; } = string.Empty;
        public string Role { get; set; } = string.Empty;
        public string VerificationToken {  get; set; } = string.Empty;
        public bool IsActive { get; set; }
        public DateTime ActivationDate {  get; set; }
        public string ResetPasswordToken { get; set; } = string.Empty;
        public bool IsResetPassword { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
        public DateTime UpdatedAt { get; set; } = DateTime.UtcNow;
    }
}
