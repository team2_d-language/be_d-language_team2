﻿using System;

namespace BE.Models
{
    public class Invoice
    {
        public string Id { get; set; } = string.Empty;

        public string InvoiceNumber { get; set; } = string.Empty;

        public DateTime Date { get; set; }

        public int TotalCourse { get; set; }

        public int TotalPrice { get; set; }

        public string PaymentId { get; set; } = string.Empty;

        public string UserId { get; set; } = string.Empty;

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
        public List<InvoiceDetails>? Detail { get; set; }
        public User? User { get; set; }
        public Payment? Payment { get; set; }
    }
}
