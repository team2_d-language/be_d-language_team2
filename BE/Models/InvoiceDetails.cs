﻿namespace BE.Models
{
    public class InvoiceDetails
    {
        public string Id { get; set; } = string.Empty;

        public string InvoiceId { get; set; } = string.Empty;

        public string CourseName { get; set; } = string.Empty;

        public string Language { get; set; } = string.Empty;

        public DateTime Schedule { get; set; }

        public int Price { get; set; }

        public string Image { get; set; } = string.Empty;

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}
