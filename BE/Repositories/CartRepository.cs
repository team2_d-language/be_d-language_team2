﻿using BE.Dtos.Cart;
using BE.Models;
using Microsoft.AspNetCore.Mvc.Razor;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.Common;

namespace BE.Repositories
{
    public class CartRepository
    {
        private readonly string _connectionString = string.Empty;
        public CartRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public string CreateCart(Cart cart)
        {
            string errorMessage = string.Empty;
            MySqlConnection conn = new MySqlConnection(_connectionString);

            conn.Open();
            MySqlTransaction transaction = conn.BeginTransaction();
            try
            {
                string checkDuplicateSql = "SELECT COUNT(*) FROM cart WHERE course_id = @course_id AND schedule = @schedule AND user_id = @user_id";
                using MySqlCommand checkDuplicateCmd = new MySqlCommand(checkDuplicateSql, conn, transaction);
                checkDuplicateCmd.Parameters.AddWithValue("@course_id", cart.CourseId);
                checkDuplicateCmd.Parameters.AddWithValue("@schedule", cart.Schedule);
                checkDuplicateCmd.Parameters.AddWithValue("@user_id", cart.UserId);

                int duplicateCount = Convert.ToInt32(checkDuplicateCmd.ExecuteScalar());

                if (duplicateCount > 0)
                {
                    errorMessage = "You have already added this course to this schedule.";
                }
                else
                {
                    cart.Id = Guid.NewGuid().ToString();
                    DateTime currentTime = DateTime.UtcNow;

                    string insertSql = "INSERT INTO cart (id, user_id, course_id, schedule, created_at, updated_at) " +
                                       "VALUES (@id, @user_id, @course_id, @schedule, @created_at, @updated_at)";

                    using MySqlCommand insertCmd = new MySqlCommand(insertSql, conn, transaction);
                    insertCmd.Parameters.AddWithValue("@id", cart.Id);
                    insertCmd.Parameters.AddWithValue("@user_id", cart.UserId);
                    insertCmd.Parameters.AddWithValue("@course_id", cart.CourseId);
                    insertCmd.Parameters.AddWithValue("@schedule", cart.Schedule);
                    insertCmd.Parameters.AddWithValue("@created_at", currentTime);
                    insertCmd.Parameters.AddWithValue("@updated_at", currentTime);

                    int rowsAffected = insertCmd.ExecuteNonQuery();
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }
            return errorMessage;
        }

        public List<GetCartDto> GetCartsByUserId(string userId)
        {
            List<GetCartDto> carts = new List<GetCartDto>();
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                string sql = "SELECT c.*, cr.name, cr.price, cr.image, ct.category_name FROM cart c " +
                             "JOIN courses cr ON cr.id = c.course_id " +
                             "JOIN categories ct ON ct.id = cr.category_id " +
                             "WHERE c.user_id = @user_id AND cr.is_active = true";

                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@user_id", userId);

                using DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    carts.Add(MapCartFromReader(reader));
                }

                return carts;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }
            return carts;
        }

        public GetCartDto? GetCartById(string id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "SELECT c.*, cr.name, cr.price, cr.image, ct.category_name FROM cart c " +
                             "JOIN courses cr ON cr.id = c.course_id " +
                             "JOIN categories ct ON ct.id = cr.category_id " +
                             "WHERE c.id = @id AND cr.is_active = true";
                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);

                using DbDataReader reader = cmd.ExecuteReader();

                GetCartDto cart = new();
                if (reader.Read())
                {
                    cart = MapCartFromReader(reader);
                }

                return cart;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public void Delete(string id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();
                string sql = "DELETE FROM cart WHERE id = @id";
                using (MySqlCommand cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    int rowsAffected = cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();
        }

        private GetCartDto MapCartFromReader(DbDataReader reader)
        {
            Console.WriteLine(reader);
            return new GetCartDto
            {
                Id = reader.GetString("id"),
                UserId = reader.GetString("user_id"),
                CourseId = reader.GetString("course_id"),
                CourseName = reader.GetString("name"),
                CoursePrice = reader.GetInt32("price"),
                CourseImage = reader.GetString("image"),
                Schedule = reader.GetDateTime("schedule"),
                CategoryName = reader.GetString("category_name")
            };
        }
    }

}
