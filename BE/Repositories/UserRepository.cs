﻿using BE.Models;
using BE.Helpers;
using MySql.Data.MySqlClient;
using System.Reflection.PortableExecutable;
using BE.Dtos.User;
using System.Transactions;
using System.Data.Common;

namespace BE.Repositories
{
    public class UserRepository
    {
        private readonly string _connectionString = string.Empty;
        public UserRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        private bool IsEmailAlreadyUsed(string email)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();

                string sql = "SELECT COUNT(*) FROM users WHERE email = @email";
                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@email", email);

                int count = Convert.ToInt32(cmd.ExecuteScalar());
                return count > 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public User? GetByToken(string token)
        {
            User? user = null;

            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT id, email, role FROM users WHERE verification_token=@Token", conn);
                cmd.Parameters.AddWithValue("@Token", token);

                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    user = new User();
                    user.Id = reader.GetString("id");
                    user.Email = reader.GetString("email");
                    user.Role = reader.GetString("role");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return user;
        }

        public bool IsEmailExist(string email)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();

                string sql = "SELECT COUNT(*) FROM users WHERE email = @email";
                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@email", email);

                int count = Convert.ToInt32(cmd.ExecuteScalar());
                return count > 0;
            }
            finally
            {
                conn.Close();
            }
        }
        private bool IsEmailAlreadyUsedByOtherUser(string userId, string email)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();

                string sql = "SELECT COUNT(*) FROM users WHERE email = @email AND id <> @id";
                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@id", userId);

                int count = Convert.ToInt32(cmd.ExecuteScalar());
                return count > 0;
            }
            finally
            {
                conn.Close();
            }
        }

        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        private bool IsValidRole(string role)
        {
            return role.ToLower() == "admin" || role.ToLower() == "user";
        }

        public List<User> GetAll()
        {
            List<User> users = new List<User>();
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "SELECT * FROM users";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    User user = new User()
                    {
                        Id = reader.GetString("id"),
                        FullName = reader.GetString("full_name"),
                        Email = reader.GetString("email"),
                        Role = reader.GetString("role"),
                        VerificationToken = reader.GetString("verification_token"),
                        IsActive = reader.GetBoolean("is_active"),
                        ActivationDate = reader.GetDateTime("activation_date"),
                        IsResetPassword = reader.GetBoolean("is_reset_password"),
                        CreatedAt = reader.GetDateTime("created_at"),
                        UpdatedAt = reader.GetDateTime("updated_at"),
                    };
                    users.Add(user);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return users;
        }

        public bool IsEmailActive(string email)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            bool isActive = false;

            try
            {
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("SELECT is_active FROM users WHERE email=@email", conn);
                cmd.Parameters.AddWithValue("@email", email);

                using MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    isActive = reader.GetBoolean("is_active");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error checking email activation status: {ex.Message}");
            }
            finally
            {
                conn.Close();
            }

            return isActive;
        }

        public User? GetByEmailAndPassword(string email, string password)
        {
            User? user = null;

            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT id, email, role FROM users WHERE email=@email and password=@password", conn);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@password", password);

                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    user = new User();
                    user.Id = reader.GetString("id");
                    user.Email = reader.GetString("email");
                    user.Role = reader.GetString("role");
                    user.VerificationToken = reader.GetString("verification_token");
                    user.IsActive = reader.GetBoolean("is_active");
                    user.ActivationDate = reader.GetDateTime("activation_date");
                    user.IsResetPassword = reader.GetBoolean("is_reset_password");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return user;
        }

        public User GetById(string id)
        {
            User user = new();
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();

                string sql = "SELECT * FROM users WHERE id=@id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                using MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    user = new User()
                    {
                        Id = reader.GetString("id"),
                        FullName = reader.GetString("full_name"),
                        Email = reader.GetString("email"),
                        Role = reader.GetString("role"),
                        VerificationToken = reader.GetString("verification_token"),
                        IsActive = reader.GetBoolean("is_active"),
                        ActivationDate = reader.GetDateTime("activation_date"),
                        IsResetPassword = reader.GetBoolean("is_reset_password"),
                        CreatedAt = reader.GetDateTime("created_at"),
                        UpdatedAt = reader.GetDateTime("updated_at"),
                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return user;
        }

        public string CreateAdmin(User newUser)
        {
            string errorMessage = string.Empty;
            MySqlConnection conn = new MySqlConnection(_connectionString);

            conn.Open();
            MySqlTransaction transaction = conn.BeginTransaction();
            try
            {
                if (IsEmailAlreadyUsed(newUser.Email))
                {
                    throw new ArgumentException("Email is already used by another user");
                }

                if (newUser.Password != newUser.ConfirmPassword)
                {
                    throw new ArgumentException("Password do not match");
                }

                newUser.Id = Guid.NewGuid().ToString();
                DateTime currentTime = DateTime.UtcNow;
                newUser.CreatedAt = currentTime;
                newUser.UpdatedAt = currentTime;

                if (!IsValidEmail(newUser.Email))
                {
                    throw new ArgumentException("Invalid email format");
                }

                //if (!IsValidRole(newUser.Role))
                //{
                //    throw new ArgumentException("Invalid role. Allowed roles are 'admin' or 'user'");
                //}

                newUser.Password = PasswordHelper.HashPassword(newUser.Password);

                string sql = "INSERT INTO users (id, full_name, email, password, role, verification_token, is_active, activation_date, reset_password_token, is_reset_password, created_at, updated_at) " +
                             "VALUES (@id, @full_name, @email, @password, 'user', 1, true, @activation_date,NULL, false, @created_at, @updated_at)";

                using MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@id", newUser.Id);
                cmd.Parameters.AddWithValue("@full_name", newUser.FullName);
                cmd.Parameters.AddWithValue("@email", newUser.Email);
                cmd.Parameters.AddWithValue("@password", newUser.Password);
                cmd.Parameters.AddWithValue("activation_date", newUser.CreatedAt);
                cmd.Parameters.AddWithValue("@created_at", newUser.CreatedAt);
                cmd.Parameters.AddWithValue("@updated_at", newUser.UpdatedAt);

                int rowsAffected = cmd.ExecuteNonQuery();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return errorMessage;
        }

        public string Register(User newUser, string verificationToken)
        {
            string errorMessage = string.Empty;
            MySqlConnection conn = new MySqlConnection(_connectionString);

            conn.Open();
            MySqlTransaction transaction = conn.BeginTransaction();
            try
            {
                if (IsEmailAlreadyUsed(newUser.Email))
                {
                    throw new ArgumentException("Email is already used by another user");
                }

                if(newUser.Password != newUser.ConfirmPassword)
                {
                    throw new ArgumentException("Password do not match");
                }

                newUser.Id = Guid.NewGuid().ToString();
                DateTime currentTime = DateTime.UtcNow;
                newUser.CreatedAt = currentTime;
                newUser.UpdatedAt = currentTime;

                if (!IsValidEmail(newUser.Email))
                {
                    throw new ArgumentException("Invalid email format");
                }

                //if (!IsValidRole(newUser.Role))
                //{
                //    throw new ArgumentException("Invalid role. Allowed roles are 'admin' or 'user'");
                //}

                newUser.Password = PasswordHelper.HashPassword(newUser.Password);

                string sql = "INSERT INTO users (id, full_name, email, password, role, verification_token, is_active, reset_password_token, is_reset_password, created_at, updated_at) " +
                             "VALUES (@id, @full_name, @email, @password, 'user', @verification_token, false, NULL, false, @created_at, @updated_at)";

                using MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@id", newUser.Id);
                cmd.Parameters.AddWithValue("@full_name", newUser.FullName);
                cmd.Parameters.AddWithValue("@email", newUser.Email);
                cmd.Parameters.AddWithValue("@password", newUser.Password);
                //cmd.Parameters.AddWithValue("@role", newUser.Role.ToLower());
                cmd.Parameters.AddWithValue("@verification_token", verificationToken);
                cmd.Parameters.AddWithValue("@created_at", newUser.CreatedAt);
                cmd.Parameters.AddWithValue("@updated_at", newUser.UpdatedAt);

                int rowsAffected = cmd.ExecuteNonQuery();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return errorMessage;
        }

        public string Activate(string userId)
        {
            string errorMessage = string.Empty;

            MySqlConnection conn = new MySqlConnection(_connectionString);

            conn.Open();

            MySqlTransaction transaction = conn.BeginTransaction();

            try
            {
                MySqlCommand cmd = new MySqlCommand("UPDATE users SET is_active=true, activation_date=@date, updated_at=@date WHERE id=@id", conn);
                cmd.Transaction = transaction;
                cmd.Parameters.AddWithValue("@id", userId);
                cmd.Parameters.AddWithValue("@date", DateTime.Now);
                cmd.ExecuteNonQuery();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }
            conn.Close();

            return errorMessage;
        }

        public string Update(string id, User userData)
        {
            string errorMessage = string.Empty;
            MySqlConnection conn = new MySqlConnection(_connectionString);

            conn.Open();
            MySqlTransaction transaction = conn.BeginTransaction();
            try
            {
                if (IsEmailAlreadyUsedByOtherUser(id, userData.Email))
                {
                    throw new ArgumentException("Email is already used by another user");
                }
                if (!IsValidEmail(userData.Email))
                {
                    throw new ArgumentException("Invalid email format");
                }

                if (!IsValidRole(userData.Role))
                {
                    throw new ArgumentException("Invalid role. Allowed roles are 'admin' or 'user'");
                }

                DateTime currentTime = DateTime.UtcNow;
                userData.UpdatedAt = currentTime;

                string sql = "UPDATE users SET full_name = @full_name, email = @email, role = @role, is_active = @is_active, updated_at = @updated_at WHERE id = @id";

                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@full_name", userData.FullName);
                cmd.Parameters.AddWithValue("@email", userData.Email);
                cmd.Parameters.AddWithValue("@role", userData.Role);
                cmd.Parameters.AddWithValue("@is_active", userData.IsActive);
                cmd.Parameters.AddWithValue("@updated_at", userData.UpdatedAt);

                int rowsAffected = cmd.ExecuteNonQuery();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }

            conn.Close();
            return errorMessage;
        }

        public void Delete(string id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();
                string sql = "DELETE FROM users WHERE id = @id";
                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();
        }

        public bool ResetPassword(string email, string token)
        {
            bool isSuccess = false;
            MySqlConnection conn = new MySqlConnection(_connectionString);
            conn.Open();
            MySqlTransaction transaction = conn.BeginTransaction();
            try
            {
                DateTime currentTime = DateTime.UtcNow;

                string sql = "UPDATE users SET reset_password_token = @token, is_reset_password = true, updated_at = @updated_at WHERE email = @email";
                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@token", token);
                cmd.Parameters.AddWithValue("@updated_at", currentTime);
                cmd.ExecuteNonQuery();
                isSuccess = true;
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                isSuccess = false;
                Console.WriteLine(ex.ToString());
            }
            conn.Close();

            return isSuccess;
        }

        public bool FindTokenResetPassword(string token)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();

                string sql = "SELECT COUNT(*) FROM users WHERE reset_password_token = @token";
                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@token", token);

                int count = Convert.ToInt32(cmd.ExecuteScalar());
                return count > 0;
            }
            finally
            {
                conn.Close();
            }
        }
        public string CreatePassword(CreatePasswordDto createPasswordBody)
        {
            string message = string.Empty;
            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                try
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    if (createPasswordBody.NewPassword != createPasswordBody.ConfirmPassword)
                    {
                        throw new ArgumentException("Password do not match");
                    }

                    DateTime currentTime = DateTime.UtcNow;

                    string sql = "SELECT is_reset_password FROM users WHERE reset_password_token = @token";
                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@token", createPasswordBody.Token);

                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            if (!reader.GetBoolean("is_reset_password"))
                            {
                                throw new ArgumentException("Reset password is not allowed");
                            }
                        }
                    }

                    string sql2 = "UPDATE users SET password = @password, is_reset_password = false, updated_at = @updated_at WHERE reset_password_token = @token";
                    using (MySqlCommand cmd2 = new MySqlCommand(sql2, conn))
                    {
                        cmd2.Parameters.AddWithValue("@token", createPasswordBody.Token);
                        cmd2.Parameters.AddWithValue("@password", PasswordHelper.HashPassword(createPasswordBody.NewPassword));
                        cmd2.Parameters.AddWithValue("@updated_at", currentTime);
                        cmd2.ExecuteNonQuery();
                    }

                    message = "Successfully reset password";
                    transaction.Commit();
                }
                catch (MySqlException ex)
                {
                    message = "An error occurred while processing your request. Please try again later.";
                    Console.WriteLine($"Error in CreatePassword: {ex}");
                }
            }

            return message;
        }
    }
}
