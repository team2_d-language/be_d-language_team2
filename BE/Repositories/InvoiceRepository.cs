﻿using BE.Dtos.Cart;
using BE.Dtos.Course;
using BE.Models;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.Common;

namespace BE.Repositories
{
    public class InvoiceRepository
    {
        private readonly string _connectionString = string.Empty;
        private readonly CartRepository _cartRepository;
        private readonly UserRepository _userRepository;
        private readonly PaymentRepository _paymentRepository;
        public InvoiceRepository(IConfiguration configuration, CartRepository cartRepository, UserRepository userRepository, PaymentRepository paymentRepository)
        {
            _connectionString = configuration.GetConnectionString("Default");
            _cartRepository = cartRepository;
            _userRepository = userRepository;
            _paymentRepository = paymentRepository;
        }

        public string generateInvoiceNumber(int counter)
        {
            const string prefix = "DLA";
            string formattedCounter = counter.ToString().PadLeft(5, '0');
            string generatedCode = $"{prefix}{formattedCounter}";
            return generatedCode;
        }
        public string CreateInvoice(string paymentId, string userId, List<string> cartIds)
        {
            string errorMessage = string.Empty;
            MySqlConnection conn = new MySqlConnection(_connectionString);

            conn.Open();
            string lockTablesSql = "LOCK TABLES invoices READ";
            using MySqlCommand lockTablesCmd = new MySqlCommand(lockTablesSql, conn);
            lockTablesCmd.ExecuteNonQuery();
            MySqlTransaction transaction = conn.BeginTransaction();
            try
            {
                // check if cart id is empty
                if (cartIds == null || cartIds.Count <= 0)
                {
                    return "Cart is empty...";
                }

                // generate id and current time
                string id = Guid.NewGuid().ToString();
                DateTime currentTime = DateTime.UtcNow;

                // initiate total price
                int totalPrice = 0;

                // find invoice count in database to generate invoice number
                string sql1 = "SELECT COUNT(*) FROM invoices";
                using MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                int counter = Convert.ToInt32(cmd1.ExecuteScalar());

                // invoice data
                var newInvoice = new Invoice
                {
                    Id = id,
                    InvoiceNumber = generateInvoiceNumber(counter + 1),
                    Date = currentTime,
                    TotalCourse = cartIds!.Count,
                    TotalPrice = totalPrice,
                    PaymentId = paymentId,
                    UserId = userId,
                    CreatedAt = currentTime,
                    UpdatedAt = currentTime,
                };

                // insert invoice
                string sql2 = "INSERT INTO invoices (id, invoice_number, date, total_course, total_price, payment_id, user_id, created_at, updated_at) " +
                              "VALUES (@id, @invoice_number, @date, @total_course, @total_price, @payment_id, @user_id, @created_at, @updated_at )";

                using MySqlCommand cmd2 = new MySqlCommand(sql2, conn);

                cmd2.Parameters.AddWithValue("@id", newInvoice.Id);
                cmd2.Parameters.AddWithValue("@invoice_number", newInvoice.InvoiceNumber);
                cmd2.Parameters.AddWithValue("@date", newInvoice.Date);
                cmd2.Parameters.AddWithValue("@total_course", newInvoice.TotalCourse);
                cmd2.Parameters.AddWithValue("@total_price", newInvoice.TotalPrice);
                cmd2.Parameters.AddWithValue("@payment_id", newInvoice.PaymentId);
                cmd2.Parameters.AddWithValue("@user_id", newInvoice.UserId);
                cmd2.Parameters.AddWithValue("@created_at", newInvoice.CreatedAt);
                cmd2.Parameters.AddWithValue("@updated_at", newInvoice.UpdatedAt);

                int rowsAffected = cmd2.ExecuteNonQuery();

                // insert invoice detail
                if (cartIds != null && cartIds.Count > 0)
                {
                    foreach (string cartId in cartIds)
                    {
                        GetCartDto? cartData = _cartRepository.GetCartById(cartId);
                        Console.WriteLine(cartData);
                        if (cartData == null || cartData.Id == "")
                        {
                            throw new ArgumentException ("Cart data not found");
                        }

                        // get total price
                        totalPrice += cartData.CoursePrice;

                        try
                        {
                            string sql = "INSERT INTO invoice_details (id, invoice_id, course_name, language, schedule, price, image, created_at, updated_at) " +
                                         "VALUES (@id, @invoice_id, @course_name, @language, @schedule, @price, @image, @created_at, @updated_at)";
                            using MySqlCommand cmd = new MySqlCommand(sql, conn);

                            cmd.Parameters.AddWithValue("@id", Guid.NewGuid().ToString());
                            cmd.Parameters.AddWithValue("@invoice_id", id);
                            cmd.Parameters.AddWithValue("@course_name", cartData.CourseName);
                            cmd.Parameters.AddWithValue("@language", cartData.CategoryName);
                            cmd.Parameters.AddWithValue("@schedule", cartData.Schedule);
                            cmd.Parameters.AddWithValue("@price", cartData.CoursePrice);
                            cmd.Parameters.AddWithValue("@image", cartData.CourseImage);
                            cmd.Parameters.AddWithValue("@created_at", currentTime);
                            cmd.Parameters.AddWithValue("@updated_at", currentTime);

                            int rowsAffected2 = cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            errorMessage = ex.Message;
                            Console.WriteLine(ex.ToString());
                            transaction.Rollback();
                        }
                    }
                }

                // update total price
                string sql3 = "UPDATE invoices SET total_price = @total_price WHERE id = @id";
                using MySqlCommand cmd3 = new MySqlCommand(sql3, conn);
                cmd3.Parameters.AddWithValue("@id", newInvoice.Id);
                cmd3.Parameters.AddWithValue("@total_price", totalPrice);
                int rowsAffected3 = cmd3.ExecuteNonQuery();

                // delete cart data
                foreach (string cartId in cartIds!)
                {
                    _cartRepository.Delete(cartId);
                }

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                string unlockTablesSql = "UNLOCK TABLES";
                using MySqlCommand unlockTablesCmd = new MySqlCommand(unlockTablesSql, conn);
                unlockTablesCmd.ExecuteNonQuery();
                conn.Close();
            }
            return errorMessage;
        }

        public string CreateInvoiceNow(string paymentId, string userId, GetAllCourseDto course, DateTime schedule)
        {
            string errorMessage = string.Empty;
            MySqlConnection conn = new MySqlConnection(_connectionString);

            conn.Open();
            string lockTablesSql = "LOCK TABLES invoices READ";
            using MySqlCommand lockTablesCmd = new MySqlCommand(lockTablesSql, conn);
            lockTablesCmd.ExecuteNonQuery();
            MySqlTransaction transaction = conn.BeginTransaction();
            try
            {
                // generate id and current time
                string id = Guid.NewGuid().ToString();
                DateTime currentTime = DateTime.UtcNow;

                // find invoice count in database to generate invoice number
                string sql1 = "SELECT COUNT(*) FROM invoices";
                using MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                int counter = Convert.ToInt32(cmd1.ExecuteScalar());

                // invoice data
                var newInvoice = new Invoice
                {
                    Id = id,
                    InvoiceNumber = generateInvoiceNumber(counter + 1),
                    Date = currentTime,
                    TotalCourse = 1,
                    TotalPrice = course.Price,
                    PaymentId = paymentId,
                    UserId = userId,
                    CreatedAt = currentTime,
                    UpdatedAt = currentTime,
                };

                // insert invoice
                string sql2 = "INSERT INTO invoices (id, invoice_number, date, total_course, total_price, payment_id, user_id, created_at, updated_at) " +
                              "VALUES (@id, @invoice_number, @date, @total_course, @total_price, @payment_id, @user_id, @created_at, @updated_at )";

                using MySqlCommand cmd2 = new MySqlCommand(sql2, conn);

                cmd2.Parameters.AddWithValue("@id", newInvoice.Id);
                cmd2.Parameters.AddWithValue("@invoice_number", newInvoice.InvoiceNumber);
                cmd2.Parameters.AddWithValue("@date", newInvoice.Date);
                cmd2.Parameters.AddWithValue("@total_course", newInvoice.TotalCourse);
                cmd2.Parameters.AddWithValue("@total_price", newInvoice.TotalPrice);
                cmd2.Parameters.AddWithValue("@payment_id", newInvoice.PaymentId);
                cmd2.Parameters.AddWithValue("@user_id", newInvoice.UserId);
                cmd2.Parameters.AddWithValue("@created_at", newInvoice.CreatedAt);
                cmd2.Parameters.AddWithValue("@updated_at", newInvoice.UpdatedAt);

                int rowsAffected = cmd2.ExecuteNonQuery();

                
                string sql = "INSERT INTO invoice_details (id, invoice_id, course_name, language, schedule, price, image, created_at, updated_at) " +
                                "VALUES (@id, @invoice_id, @course_name, @language, @schedule, @price, @image, @created_at, @updated_at)";
                using MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@id", Guid.NewGuid().ToString());
                cmd.Parameters.AddWithValue("@invoice_id", id);
                cmd.Parameters.AddWithValue("@course_name", course.Name);
                cmd.Parameters.AddWithValue("@language", course.CategoryName);
                cmd.Parameters.AddWithValue("@schedule", schedule);
                cmd.Parameters.AddWithValue("@price", course.Price);
                cmd.Parameters.AddWithValue("@image", course.Image);
                cmd.Parameters.AddWithValue("@created_at", currentTime);
                cmd.Parameters.AddWithValue("@updated_at", currentTime);

                int rowsAffected2 = cmd.ExecuteNonQuery();
                
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                string unlockTablesSql = "UNLOCK TABLES";
                using MySqlCommand unlockTablesCmd = new MySqlCommand(unlockTablesSql, conn);
                unlockTablesCmd.ExecuteNonQuery();
                conn.Close();
            }
            return errorMessage;
        }

        public List<InvoiceDetails> GetInvoiceDetails(string invoiceId)
        {
            List<InvoiceDetails> invoiceDetails = new List<InvoiceDetails>();
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                string sql2 = "SELECT * FROM invoice_details WHERE invoice_id=@invoice_id ORDER BY created_at DESC";
                using MySqlCommand cmd2 = new MySqlCommand(sql2, conn);
                cmd2.Parameters.AddWithValue("@invoice_id", invoiceId);

                using DbDataReader reader2 = cmd2.ExecuteReader();

                while (reader2.Read())
                {
                    invoiceDetails.Add(MapInvoiceDetailsFromReader(reader2));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }
            return invoiceDetails;
        }

        public List<Invoice> GetAll()
        {
            List<Invoice> invoices = new List<Invoice>();
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                string sql = "SELECT i.* FROM invoices i JOIN users u ON u.id = i.user_id WHERE u.role = 'user' ORDER BY i.created_at DESC";

                using MySqlCommand cmd = new MySqlCommand(sql, conn);

                using DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    invoices.Add(MapInvoiceFromReader(reader));
                }

                // get invoice details
                for (int i = 0; i < invoices.Count; i++)
                {
                    invoices[i].Detail = GetInvoiceDetails(invoices[i].Id);
                    invoices[i].User = _userRepository.GetById(invoices[i].UserId);
                    invoices[i].Payment = _paymentRepository.GetById(invoices[i].PaymentId);
                }

                return invoices;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }
            return invoices;
        }

        public List<Invoice> GetAllByUserId(string userId)
        {
            List<Invoice> invoices = new List<Invoice>();
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                string sql = "SELECT * FROM invoices WHERE user_id = @user_id ORDER BY created_at DESC";

                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@user_id", userId);

                using DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    invoices.Add(MapInvoiceFromReader(reader));
                }

                // get invoice details
                for (int i = 0; i < invoices.Count; i++)
                {
                    invoices[i].Detail = GetInvoiceDetails(invoices[i].Id);
                }

                return invoices;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }
            return invoices;
        }

        public Invoice GetInvoiceById(string invoiceId)
        {
            Invoice invoice = new Invoice();
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                string sql = "SELECT * FROM invoices WHERE id = @id";

                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", invoiceId);

                using DbDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    invoice = MapInvoiceFromReader(reader);
                }

                invoice.Detail = GetInvoiceDetails(invoice.Id);

                return invoice;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }
            return invoice;
        }

        private Invoice MapInvoiceFromReader(DbDataReader reader)
        {
            Console.WriteLine(reader);
            return new Invoice
            {
                Id = reader.GetString("id"),
                InvoiceNumber = reader.GetString("invoice_number"),
                Date = reader.GetDateTime("date"),
                TotalCourse = reader.GetInt32("total_course"),
                TotalPrice = reader.GetInt32("total_price"),
                PaymentId = reader.GetString("payment_id"),
                UserId = reader.GetString("user_id"),
                CreatedAt = reader.GetDateTime("created_at"),
                UpdatedAt = reader.GetDateTime("updated_at"),
            };
        }
        private InvoiceDetails MapInvoiceDetailsFromReader(DbDataReader reader)
        {
            Console.WriteLine(reader);
            return new InvoiceDetails
            {
                Id = reader.GetString("id"),
                InvoiceId = reader.GetString("invoice_id"),
                CourseName = reader.GetString("course_name"),
                Language = reader.GetString("language"),
                Schedule = reader.GetDateTime("schedule"),
                Price = reader.GetInt32("price"),
                Image = reader.GetString("image"),
                CreatedAt = reader.GetDateTime("created_at"),
                UpdatedAt = reader.GetDateTime("updated_at"),
            };
        }
    }
}
