﻿using BE.Dtos.Payment;
using BE.Models;
using MySql.Data.MySqlClient;

namespace BE.Repositories
{
    public class PaymentRepository
    {
        private readonly string _connectionString = string.Empty;
        public PaymentRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public List<Payment> GetAll()
        {
            List<Payment> payments = new List<Payment>();
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "SELECT * FROM payments";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Payment payment = new Payment()
                    {
                        Id = reader.GetString("id"),
                        PaymentMethod = reader.GetString("payment_method"),
                        Logo = reader.GetString("logo"),
                        IsActive = reader.GetBoolean("is_active"),
                        CreatedAt = reader.GetDateTime("created_at"),
                        UpdatedAt = reader.GetDateTime("updated_at"),
                    };

                    payments.Add(payment);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return payments;
        }

        public List<Payment> GetAllActive()
        {
            List<Payment> payments = new List<Payment>();
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "SELECT * FROM payments WHERE is_active = true";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Payment payment = new Payment()
                    {
                        Id = reader.GetString("id"),
                        PaymentMethod = reader.GetString("payment_method"),
                        Logo = reader.GetString("logo"),
                        IsActive = reader.GetBoolean("is_active"),
                        CreatedAt = reader.GetDateTime("created_at"),
                        UpdatedAt = reader.GetDateTime("updated_at"),
                    };

                    payments.Add(payment);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return payments;
        }

        public Payment GetById(string id)
        {
            Payment payment = new();
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();

                string sql = "SELECT * FROM payments WHERE id=@id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);

                using MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    payment = new Payment()
                    {
                        Id = reader.GetString("id"),
                        PaymentMethod = reader.GetString("payment_method"),
                        Logo = reader.GetString("logo"),
                        IsActive = reader.GetBoolean("is_active"),
                        CreatedAt = reader.GetDateTime("created_at"),
                        UpdatedAt = reader.GetDateTime("updated_at"),
                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return payment;
        }

        public string Create(Payment newPayment)
        {
            string errorMessage = string.Empty;
            MySqlConnection conn = new MySqlConnection(_connectionString);

            conn.Open();
            MySqlTransaction transaction = conn.BeginTransaction();
            try
            {
                newPayment.Id = Guid.NewGuid().ToString();
                DateTime currentTime = DateTime.UtcNow;

                string sql = "INSERT INTO payments (id, payment_method, logo, is_active, created_at, updated_at) " +
                             "VALUES (@id, @payment_method, @logo, @is_active, @created_at, @updated_at)";

                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", newPayment.Id);
                cmd.Parameters.AddWithValue("@payment_method", newPayment.PaymentMethod);
                cmd.Parameters.AddWithValue("@logo", newPayment.Logo);
                cmd.Parameters.AddWithValue("@is_active", newPayment.IsActive);
                cmd.Parameters.AddWithValue("@created_at", currentTime);
                cmd.Parameters.AddWithValue("@updated_at", currentTime);

                int rowsAffected = cmd.ExecuteNonQuery();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return errorMessage;
        }

        public string Update(string id, Payment paymentData)
        {
            string errorMessage = string.Empty;
            MySqlConnection conn = new MySqlConnection(_connectionString);

            conn.Open();
            MySqlTransaction transaction = conn.BeginTransaction();
            try
            {
                DateTime currentTime = DateTime.UtcNow;

                string sql = "UPDATE payments SET payment_method = @payment_method, logo = @logo, is_active = @is_active, updated_at = @updated_at WHERE id = @id";

                using (MySqlCommand cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@payment_method", paymentData.PaymentMethod);
                    cmd.Parameters.AddWithValue("@logo", paymentData.Logo);
                    cmd.Parameters.AddWithValue("@is_active", paymentData.IsActive);
                    cmd.Parameters.AddWithValue("@updated_at", currentTime);

                    int rowsAffected = cmd.ExecuteNonQuery();
                }
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }

            conn.Close();
            return errorMessage;
        }

        public void Delete(string id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();
                string sql = "DELETE FROM payments WHERE id = @id";
                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                int rowsAffected = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();
        }
    }
}
