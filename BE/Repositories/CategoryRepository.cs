﻿using BE.Dtos.Course;
using BE.Models;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.Common;
using System.Net;
using System.Transactions;

namespace BE.Repositories
{
    public class CategoryRepository
    {
        private readonly string _connectionString = string.Empty;
        public CategoryRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public List<Category> GetAll()
        {
            List<Category> categories = new List<Category>();
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "SELECT * FROM categories";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Category category = new Category()
                    {
                        Id = reader.GetString("id"),
                        CategoryName = reader.GetString("category_name"),
                        Description = reader.GetString("description"),
                        IsActive = reader.GetBoolean("is_active"),
                        Banner = reader.IsDBNull("banner") ? string.Empty : reader.GetString("banner"),
                        Flag = reader.IsDBNull("flag") ? string.Empty : reader.GetString("flag"),
                        CreatedAt = reader.GetDateTime("created_at"),
                        UpdatedAt = reader.GetDateTime("updated_at"),
                    };

                    categories.Add(category);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return categories;
        }

        public List<Category> GetAllActive()
        {
            List<Category> categories = new List<Category>();
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "SELECT * FROM categories WHERE is_active = true";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Category category = new Category()
                    {
                        Id = reader.GetString("id"),
                        CategoryName = reader.GetString("category_name"),
                        Description = reader.GetString("description"),
                        Banner = reader.IsDBNull("banner") ? string.Empty : reader.GetString("banner"),
                        Flag = reader.IsDBNull("flag") ? string.Empty : reader.GetString("flag"),
                    };

                    categories.Add(category);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return categories;
        }

        public Category GetByIdActive(string id)
        {
            Category category = new();
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();

                string sql = "SELECT * FROM categories WHERE id=@id AND is_active = true";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                using MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    category = new Category()
                    {
                        Id = reader.GetString("id"),
                        CategoryName = reader.GetString("category_name"),
                        Description = reader.GetString("description"),
                        IsActive = reader.GetBoolean("is_active"),
                        Banner = reader.IsDBNull("banner") ? string.Empty : reader.GetString("banner"),
                        Flag = reader.IsDBNull("flag") ? string.Empty : reader.GetString("flag"),
                        CreatedAt = reader.GetDateTime("created_at"),
                        UpdatedAt = reader.GetDateTime("updated_at"),
                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return category;
        }

        public Category GetById(string id)
        {
            Category category = new();
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();

                string sql = "SELECT * FROM categories WHERE id=@id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                using MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    category = new Category()
                    {
                        Id = reader.GetString("id"),
                        CategoryName = reader.GetString("category_name"),
                        Description = reader.GetString("description"),
                        IsActive = reader.GetBoolean("is_active"),
                        Banner = reader.IsDBNull("banner") ? string.Empty : reader.GetString("banner"),
                        Flag = reader.IsDBNull("flag") ? string.Empty : reader.GetString("flag"),
                        CreatedAt = reader.GetDateTime("created_at"),
                        UpdatedAt = reader.GetDateTime("updated_at"),
                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return category;
        }

        public string Create(Category newCategory)
        {
            string errorMessage = string.Empty;
            MySqlConnection conn = new MySqlConnection(_connectionString);

            conn.Open();
            MySqlTransaction transaction = conn.BeginTransaction();
            try
            {
                newCategory.Id = Guid.NewGuid().ToString();
                DateTime currentTime = DateTime.UtcNow;
                newCategory.CreatedAt = currentTime;
                newCategory.UpdatedAt = currentTime;

                string sql = "INSERT INTO categories (id, category_name, banner, flag, description, is_active, created_at, updated_at) " +
                             "VALUES (@id, @category_name, @banner, @flag, @description, true, @created_at, @updated_at)";

                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", newCategory.Id);
                cmd.Parameters.AddWithValue("@category_name", newCategory.CategoryName);
                cmd.Parameters.AddWithValue("@banner", newCategory.Banner);
                cmd.Parameters.AddWithValue("@flag", newCategory.Flag);
                cmd.Parameters.AddWithValue("@description", newCategory.Description);
                cmd.Parameters.AddWithValue("@created_at", newCategory.CreatedAt);
                cmd.Parameters.AddWithValue("@updated_at", newCategory.UpdatedAt);

                int rowsAffected = cmd.ExecuteNonQuery();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return errorMessage;
        }

        public string Update(string id, Category categoryData)
        {
            string errorMessage = string.Empty;
            MySqlConnection conn = new MySqlConnection(_connectionString);

            conn.Open();
            MySqlTransaction transaction = conn.BeginTransaction();
            try
            {

                DateTime currentTime = DateTime.UtcNow;
                categoryData.UpdatedAt = currentTime;

                string sql = "UPDATE categories SET category_name = @category_name, banner = @banner, flag = @flag, description = @description, is_active = @is_active, updated_at = @updated_at WHERE id = @id";

                using (MySqlCommand cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@category_name", categoryData.CategoryName);
                    cmd.Parameters.AddWithValue("@banner", categoryData.Banner);
                    cmd.Parameters.AddWithValue("@flag", categoryData.Flag);
                    cmd.Parameters.AddWithValue("@description", categoryData.Description);
                    cmd.Parameters.AddWithValue("@is_active", categoryData.IsActive);
                    cmd.Parameters.AddWithValue("@updated_at", categoryData.UpdatedAt);

                    int rowsAffected = cmd.ExecuteNonQuery();
                }
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }

            conn.Close();
            return errorMessage;
        }

        public void Delete(string id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();
                string sql = "DELETE FROM categories WHERE id = @id";
                using (MySqlCommand cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    int rowsAffected = cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();
        }
    }
}
