﻿using BE.Dtos.Course;
using BE.Models;
using MySql.Data.MySqlClient;
using System.Data;
using System.Reflection.PortableExecutable;

namespace BE.Repositories
{
    public class CourseRepository
    {
        private readonly string _connectionString = string.Empty;
        public CourseRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public List<Course> GetAll()
        {
            List<Course> courses = new List<Course>();
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "SELECT * FROM courses";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Course course = new Course()
                    {
                        Id = reader.GetString("id"),
                        Name = reader.GetString("name"),
                        Category_id = reader.GetString("category_id"),
                        Description = reader.GetString("description"),
                        Price = reader.GetInt32("price"),
                        Image = reader.IsDBNull("image") ? string.Empty : reader.GetString("image"),
                        IsActive = reader.GetBoolean("is_active"),
                        CreatedAt = reader.GetDateTime("created_at"),
                        UpdatedAt = reader.GetDateTime("updated_at"),
                    };
                    courses.Add(course);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return courses;
        }

        public List<GetAllCourseDto> GetAllActive()
        {
            List<GetAllCourseDto> courses = new List<GetAllCourseDto>();
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "SELECT c.*, ct.category_name, ct.banner, ct.flag FROM courses c " +
                             "JOIN categories ct ON c.category_id = ct.id " +
                             "WHERE c.is_active = true";

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    GetAllCourseDto course = new GetAllCourseDto()
                    {
                        Id = reader.GetString("id"),
                        Name = reader.GetString("name"),
                        Category_id = reader.GetString("category_id"),
                        Description = reader.GetString("description"),
                        Price = reader.GetInt32("price"),
                        Image = reader.IsDBNull("image") ? string.Empty : reader.GetString("image"),
                        IsActive = reader.GetBoolean("is_active"),
                        CreatedAt = reader.GetDateTime("created_at"),
                        UpdatedAt = reader.GetDateTime("updated_at"),
                    };

                    course.CategoryName = reader.GetString("category_name");
                    course.CategoryBanner = reader.GetString("banner");
                    course.CategoryFlag = reader.GetString("flag");

                    courses.Add(course);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return courses;
        }

        public List<GetAllCourseDto> GetAllByCategoryActive(string category)
        {
            List<GetAllCourseDto> courses = new List<GetAllCourseDto>();
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "SELECT c.*, ct.category_name, ct.banner, ct.flag FROM courses c " +
                             "JOIN categories ct ON c.category_id = ct.id " +
                             "WHERE ct.category_name = @category AND c.is_active = true";
                
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@category", category);

                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    GetAllCourseDto course = new GetAllCourseDto()
                    {
                        Id = reader.GetString("id"),
                        Name = reader.GetString("name"),
                        Category_id = reader.GetString("category_id"),
                        Description = reader.GetString("description"),
                        Price = reader.GetInt32("price"),
                        Image = reader.IsDBNull("image") ? string.Empty : reader.GetString("image"),
                        IsActive = reader.GetBoolean("is_active"),
                    };

                    course.CategoryName = reader.GetString("category_name");
                    course.CategoryBanner = reader.GetString("banner");
                    course.CategoryFlag = reader.GetString("flag");

                    courses.Add(course);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return courses;
        }

        public List<GetAllCourseDto> GetAllByCategory(string category)
        {
            List<GetAllCourseDto> courses = new List<GetAllCourseDto>();
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "SELECT c.*, ct.category_name, ct.banner, ct.flag FROM courses c " +
                             "JOIN categories ct ON c.category_id = ct.id " +
                             "WHERE ct.category_name = @category";

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@category", category);

                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    GetAllCourseDto course = new GetAllCourseDto()
                    {
                        Id = reader.GetString("id"),
                        Name = reader.GetString("name"),
                        Category_id = reader.GetString("category_id"),
                        Description = reader.GetString("description"),
                        Price = reader.GetInt32("price"),
                        Image = reader.IsDBNull("image") ? string.Empty : reader.GetString("image"),
                        IsActive = reader.GetBoolean("is_active"),
                    };

                    course.CategoryName = reader.GetString("category_name");
                    course.CategoryBanner = reader.GetString("banner");
                    course.CategoryFlag = reader.GetString("flag");

                    courses.Add(course);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return courses;
        }

        public GetAllCourseDto GetByIdActive(string id)
        {
            GetAllCourseDto course = new();
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();

                string sql = "SELECT c.*, ct.category_name, ct.banner, ct.flag FROM courses c " +
                             "JOIN categories ct ON c.category_id = ct.id " +
                             "WHERE c.id=@id AND c.is_active = true";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                using MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    course = new GetAllCourseDto()
                    {
                        Id = reader.GetString("id"),
                        Name = reader.GetString("name"),
                        Category_id = reader.GetString("category_id"),
                        Description = reader.GetString("description"),
                        Price = reader.GetInt32("price"),
                        Image = reader.IsDBNull("image") ? string.Empty : reader.GetString("image"),
                        IsActive = reader.GetBoolean("is_active"),
                        CreatedAt = reader.GetDateTime("created_at"),
                        UpdatedAt = reader.GetDateTime("updated_at"),
                    };
                    course.CategoryName = reader.GetString("category_name");
                    course.CategoryBanner = reader.GetString("banner");
                    course.CategoryFlag = reader.GetString("flag");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return course;
        }

        public GetAllCourseDto GetById(string id)
        {
            GetAllCourseDto course = new();
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();

                string sql = "SELECT c.*, ct.category_name, ct.banner, ct.flag FROM courses c " +
                             "JOIN categories ct ON c.category_id = ct.id " +
                             "WHERE c.id=@id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                using MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    course = new GetAllCourseDto()
                    {
                        Id = reader.GetString("id"),
                        Name = reader.GetString("name"),
                        Category_id = reader.GetString("category_id"),
                        Description = reader.GetString("description"),
                        Price = reader.GetInt32("price"),
                        Image = reader.IsDBNull("image") ? string.Empty : reader.GetString("image"),
                        IsActive = reader.GetBoolean("is_active"),
                        CreatedAt = reader.GetDateTime("created_at"),
                        UpdatedAt = reader.GetDateTime("updated_at"),
                    };
                    course.CategoryName = reader.GetString("category_name");
                    course.CategoryBanner = reader.GetString("banner");
                    course.CategoryFlag = reader.GetString("flag");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return course;
        }

        private bool IsCategoryExist(string categoryId)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();

                string sql = "SELECT COUNT(*) FROM categories WHERE id = @id";
                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", categoryId);

                int count = Convert.ToInt32(cmd.ExecuteScalar());
                return count > 0;
            }
            finally
            {
                conn.Close();
            }
        }
        public string Create(Course newCourse)
        {
            string errorMessage = string.Empty;
            MySqlConnection conn = new MySqlConnection(_connectionString);

            conn.Open();
            MySqlTransaction transaction = conn.BeginTransaction();
            try
            {
                if (!IsCategoryExist(newCourse.Category_id))
                {
                    throw new ArgumentException("Category not found");
                }
                newCourse.Id = Guid.NewGuid().ToString();
                DateTime currentTime = DateTime.UtcNow;

                string sql = "INSERT INTO courses (id, name, category_id, description, price, image, is_active, created_at, updated_at) " +
                             "VALUES (@id, @name, @category_id, @description, @price, @image, true, @created_at, @updated_at)";

                using MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@id", newCourse.Id);
                cmd.Parameters.AddWithValue("@name", newCourse.Name);
                cmd.Parameters.AddWithValue("@category_id", newCourse.Category_id);
                cmd.Parameters.AddWithValue("@description", newCourse.Description);
                cmd.Parameters.AddWithValue("@price", newCourse.Price);
                cmd.Parameters.AddWithValue("@image", newCourse.Image);
                cmd.Parameters.AddWithValue("@created_at", currentTime);
                cmd.Parameters.AddWithValue("@updated_at", currentTime);

                int rowsAffected = cmd.ExecuteNonQuery();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }

            conn.Close();

            return errorMessage;
        }

        public string Update(string id, Course courseData)
        {
            string errorMessage = string.Empty;
            MySqlConnection conn = new MySqlConnection(_connectionString);

            conn.Open();
            MySqlTransaction transaction = conn.BeginTransaction();
            try
            {
                DateTime currentTime = DateTime.UtcNow;
                
                string sql = "UPDATE courses SET name = @name, category_id = @category_id, description = @description, price = @price, image = @image, is_active = @is_active, updated_at = @updated_at WHERE id = @id";

                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@name", courseData.Name);
                cmd.Parameters.AddWithValue("@category_id", courseData.Category_id);
                cmd.Parameters.AddWithValue("@description", courseData.Description);
                cmd.Parameters.AddWithValue("@price", courseData.Price);
                cmd.Parameters.AddWithValue("@image", courseData.Image);
                cmd.Parameters.AddWithValue("@is_active", courseData.IsActive);
                cmd.Parameters.AddWithValue("@updated_at", currentTime);

                int rowsAffected = cmd.ExecuteNonQuery();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }

            conn.Close();
            return errorMessage;
        }

        public void Delete(string id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();
                string sql = "DELETE FROM courses WHERE id = @id";
                using MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();
        }
    }
}
