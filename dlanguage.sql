-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2024 at 03:24 PM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dlanguage`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `course_id` varchar(36) NOT NULL,
  `schedule` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` varchar(36) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `banner` varchar(255) NOT NULL,
  `flag` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `banner`, `flag`, `description`, `is_active`, `created_at`, `updated_at`) VALUES
('0561fbbe-1926-423b-beb0-8a4e4713a8ab', 'Mandarin', 'uploads/3b8ee1d2-6a59-4ef9-8a21-1d3c5dcfb781.jpg', 'uploads/73d0af7a-5f2a-44c6-bc5c-4a0f8d95915e.svg', 'Discover the beauty and richness of the Mandarin language with our Mandarin courses. Immerse yourself in the fascinating world of Chinese characters, tones, and culture. Whether you\'re a beginner or looking to enhance your language skills, our courses cater to all levels of proficiency.', 1, '2023-12-21 10:11:10', '2024-01-07 14:18:24'),
('4cf97b3b-54a3-4388-9673-4d009c2dcff0', 'Arabic', 'uploads/e4337f23-6e4b-4d3c-8d15-7e59fc62967c.jpeg', 'uploads/242d95a9-68c6-452c-8678-88cb1a8a1d0e.svg', 'Embark on a linguistic journey with our Arabic courses. From the beauty of the Arabic script to the intricacies of grammar, our courses provide a comprehensive learning experience. Join us and explore the richness of the Arabic language and culture.', 1, '2023-12-21 10:09:51', '2024-01-07 14:18:49'),
('581bdae2-298e-4a3e-b5fc-235329f45230', 'Japanese', 'uploads/bce0d3e1-3db6-42a6-af41-c145e4e3f41e.jpeg', 'uploads/3c6f74f6-c0cd-48ed-8617-91b86e1d5fb7.svg', 'Embark on a journey of linguistic and cultural exploration with our Japanese courses. Learn the art of Kanji, explore traditional customs, and master the nuances of the Japanese language. Join us and unlock the doors to the captivating world of Japan.', 1, '2023-12-21 10:13:02', '2024-01-07 14:19:21'),
('66884196-cc79-4d59-b578-e54e66952c75', 'Melayu', 'uploads/81040455-ad28-40a0-bc73-b064c392c6db.jpg', 'uploads/642de694-8cbd-442c-a89d-4c5f11c891ca.svg', 'Dive into the linguistic richness of the Malay language with our Melayu courses. From basic vocabulary to cultural nuances, our courses provide a holistic learning experience. Join us and explore the diverse aspects of the Malay language and culture.', 1, '2023-12-21 10:13:39', '2024-01-07 14:19:47'),
('ac49578c-daae-4390-bc1b-e3f1bc8a7faa', 'Deutsch', 'uploads/f72f3ae4-3813-4284-95ca-f3b90e23582b.jpeg', 'uploads/350294db-10e4-4878-ab84-4f86e2d59461.svg', 'Embark on a linguistic journey with our Deutsch courses. From the basics of German grammar to conversational proficiency, our courses cater to learners of all levels. Join us and immerse yourself in the beauty of the German language and culture.', 1, '2023-12-21 10:11:27', '2024-01-07 14:20:05'),
('cbda40d2-7639-467a-8802-c9c7d4d984c1', 'English', 'uploads/8b41abf2-c9f4-4740-97ee-8d2b5763c046.png', 'uploads/74141044-2925-49fc-95b1-f7136efee304.svg', 'Enhance your English language skills with our comprehensive English courses. From grammar fundamentals to advanced proficiency, our courses are designed to cater to learners of all levels. Join us and unlock the doors to effective communication in English.', 1, '2023-12-21 10:14:26', '2024-01-07 14:20:47'),
('e3bca440-6593-45e7-9705-f7f497ece3ca', 'Indonesian', 'uploads/680bb159-8a56-4d7f-8e9b-0f7e5502ee60.jpg', 'uploads/6b0d1ebb-59b7-4de1-831f-0b398f07ea30.svg', 'Discover the beauty of Bahasa Indonesia with our Indonesian language courses. Whether you\'re a beginner or looking to enhance your proficiency, our courses cover a range of topics from basic conversation to advanced language skills. Join us and explore the richness of Indonesian culture through language.', 1, '2023-12-21 10:12:47', '2024-01-07 14:21:16'),
('fd0661d9-0c8d-4f1b-bf4e-afbe5dcbf354', 'French', 'uploads/917f59e0-39fb-4114-9ed8-57c842db5d13.webp', 'uploads/ca638ae8-f74c-439a-bf8a-58e9b01371ed.svg', 'Immerse yourself in the elegance of the French language with our French courses. From basic vocabulary to advanced conversation, our courses offer a comprehensive learning experience. Join us and explore the beauty of French language and culture.', 1, '2023-12-21 10:12:27', '2024-01-07 14:21:37');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category_id` varchar(36) NOT NULL,
  `description` text NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `name`, `category_id`, `description`, `price`, `image`, `is_active`, `created_at`, `updated_at`) VALUES
('78ad1884-548e-4baa-8092-d16716914f6d', 'Germany Language for Junior', 'ac49578c-daae-4390-bc1b-e3f1bc8a7faa', 'This course is designed for beginners who want to learn the German language. You will start with the basics of vocabulary, grammar, and pronunciation. By the end of the course, you will be able to introduce yourself, have basic conversations, and understand simple texts in German.', 450000, 'uploads/d50fd342-2381-44ce-bcda-3a55e8743580.svg', 1, '2023-12-21 10:27:06', '2024-01-07 14:10:48'),
('8bb9d3b6-2049-46c9-93a8-ea9e8bab6317', 'Arabic Course - Beginner to Middle', '4cf97b3b-54a3-4388-9673-4d009c2dcff0', 'Embark on a journey to learn Arabic from scratch. This course covers the fundamental aspects of the Arabic language, including the alphabet, pronunciation, and basic vocabulary. Suitable for beginners who want to explore the richness of the Arabic language.', 550000, 'uploads/b94d9859-239b-46bf-8e87-e5e78bcf0dbd.svg', 1, '2023-12-21 10:25:51', '2024-01-07 14:11:13'),
('9e00601a-dc4b-4d91-bea2-7e39cfd1b2af', 'Kursus Bahasa Indonesia', 'e3bca440-6593-45e7-9705-f7f497ece3ca', 'Master the Indonesian language with this comprehensive course. From everyday phrases to advanced conversations, you will delve into the linguistic and cultural aspects of Bahasa Indonesia. Whether you\'re a beginner or looking to enhance your language skills, this course is tailored for you.', 650000, 'uploads/cc97d1f8-b44b-486c-8944-e1fe9735ebdb.svg', 1, '2023-12-21 10:26:19', '2024-01-07 14:14:17'),
('b5dba4d0-0e9b-4255-97c0-e6fe3c48e265', 'Complit Package - Expert English, TOEFL and IELT', 'cbda40d2-7639-467a-8802-c9c7d4d984c1', 'Take your English proficiency to the next level with this expert-level course. Covering advanced grammar, vocabulary, and test preparation for TOEFL and IELTS, this package is ideal for those aiming for high proficiency and success in English language exams.', 2000000, 'uploads/3dbd8d83-3817-4cbb-8c61-472ea6ab60b1.svg', 1, '2023-12-21 10:24:19', '2024-01-07 14:13:41'),
('d6d8c9f5-917b-4cf2-b2ec-b86269911b25', 'Level 1 Mandarin', '0561fbbe-1926-423b-beb0-8a4e4713a8ab', 'Start your Mandarin learning journey with Level 1 of this course. You will cover essential vocabulary, basic grammar, and useful phrases. Gain a solid foundation in Mandarin Chinese and lay the groundwork for further language proficiency.', 200000, 'uploads/d30b739d-4fe2-438e-9394-9c42a08a50e0.svg', 1, '2023-12-21 10:24:45', '2024-01-07 14:15:25'),
('e6526cb2-40d6-42fc-841b-0e557fe5bc6d', 'Basic English for Junior', 'cbda40d2-7639-467a-8802-c9c7d4d984c1', 'Designed for young learners, this course focuses on building English language skills through interactive activities, games, and engaging lessons. Help your child develop a strong foundation in English in a fun and supportive learning environment.', 400000, 'uploads/ede2e8bb-c8a8-4aa6-a33c-ee2f35c146f3.svg', 1, '2023-12-21 10:20:26', '2024-01-07 14:15:56');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` varchar(36) NOT NULL,
  `invoice_number` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `total_course` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `payment_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details`
--

CREATE TABLE `invoice_details` (
  `id` varchar(36) NOT NULL,
  `invoice_id` varchar(36) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `schedule` datetime NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` varchar(36) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `payment_method`, `logo`, `is_active`, `created_at`, `updated_at`) VALUES
('0deabbe6-65b9-4479-945d-04e74e316e60', 'Ovo', 'uploads/ed77e204-2598-40bf-8344-f93f3fd1239a.svg', 1, '2023-12-28 03:56:52', '2024-01-07 14:22:45'),
('28e20830-a6b6-4889-ae4b-1f0aff4e995e', 'BNI', 'uploads/b011f5dc-11e1-4a1b-a874-5f0e42ac44af.svg', 1, '2023-12-28 03:57:37', '2024-01-07 14:22:49'),
('7efa2f1b-a830-4002-9e33-1c0a7f16673d', 'Gopay', 'uploads/f35e6323-ba65-4799-8a4e-74927e22bb13.svg', 1, '2023-12-28 03:56:18', '2024-01-07 14:22:55'),
('cbf8744a-cd4a-41cd-808e-91f0fb931380', 'Mandiri', 'uploads/50949f5e-c06a-4f7c-9d6a-407a98774cbc.svg', 1, '2023-12-28 03:57:16', '2024-01-07 14:23:05'),
('d3c07709-7961-4972-8631-f5ec9ef2de60', 'BCA', 'uploads/f8196e0c-712f-48ce-b593-dc3e30deae3b.svg', 1, '2023-12-28 03:57:25', '2024-01-07 14:23:11'),
('db31d908-affe-4bb2-9451-b6ee9b868278', 'Dana', 'uploads/02a844c4-a999-4024-8710-92fdbfbd6392.svg', 1, '2023-12-28 03:57:05', '2024-01-07 14:23:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(36) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `role` varchar(255) NOT NULL,
  `verification_token` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `activation_date` datetime DEFAULT NULL,
  `reset_password_token` varchar(255) DEFAULT NULL,
  `is_reset_password` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `password`, `created_at`, `updated_at`, `role`, `verification_token`, `is_active`, `activation_date`, `reset_password_token`, `is_reset_password`) VALUES
('0a8acde0-2fb3-48a3-949a-ac5da4f39cc4', 'admin', 'info.dlanguage@gmail.com', '4CC19AAFF82F60AC4097F935AB4A06AD4F0891CC', '2023-12-29 05:42:51', '2024-01-01 05:13:46', 'admin', '5d71c74c-3a4b-49dc-9137-4bf95abcad96', 1, '2023-12-29 12:43:14', NULL, 0),
('7739b031-f7b7-4de9-8695-907731608713', 'Felix', 'felixfdnd@gmail.com', '5FA339BBBB1EEACED3B52E54F44576AAF0D77D96', '2024-01-05 11:57:17', '2024-01-05 12:25:32', 'user', '8ef9db9f-cb10-4d4a-b8d0-c04978d8af8e', 1, '2024-01-05 18:57:35', 'a1bd7c94-433b-48b0-8c22-7700fad56796', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `course_id` (`course_id`),
  ADD KEY `schedule_id` (`schedule`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_number` (`invoice_number`),
  ADD KEY `payment_id` (`payment_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_id` (`invoice_id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`);

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_ibfk_1` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`),
  ADD CONSTRAINT `invoices_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD CONSTRAINT `invoice_details_ibfk_1` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
